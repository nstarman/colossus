=========
Tutorials
=========

The easiest way to learn how to use Colossus is by following the tutorial notebooks. You can learn more about Jupyter notebooks on the `Jupyter website <http://jupyter.org/>`_. The editable notebooks are located in the ``colossus/tutorials`` directory, the links below point to their static html versions:

* `Cosmology <_static/tutorial_cosmology.html>`_
* `LSS: peaks in Gaussian random fields <_static/tutorial_lss_peaks.html>`_
* `LSS: mass function <_static/tutorial_lss_mass_function.html>`_
* `LSS: halo bias <_static/tutorial_lss_bias.html>`_
* `Halo: spherical overdensity masses and radii <_static/tutorial_halo_so.html>`_
* `Halo: density profiles <_static/tutorial_halo_profile.html>`_
* `Halo: concentration <_static/tutorial_halo_concentration.html>`_
* `Halo: splashback <_static/tutorial_halo_splashback.html>`_
* `Utilities: MCMC <_static/tutorial_utils_mcmc.html>`_

The :ref:`search` is useful if you are looking for examples of particular functions.